const express = require('express')
const api = express.Router()

api.get("/planestats",(req,res)=>{
    res.render("stats/planestats.ejs");
})
api.get("/flightstats",(req,res)=>{
    res.render("stats/flightstats.ejs");
})
api.get("/pilotstats",(req,res)=>{
    res.render("stats/pilotstats.ejs");
})


module.exports = api